package rfctks.dualmodemc.bukkitcore.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import rfctks.dualmodemc.bukkitcore.DualModeMC;
import rfctks.dualmodemc.bukkitcore.PlayerStates;
import rfctks.dualmodemc.bukkitcore.api.DualModeMCTracker;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PlayerListener implements Listener {
    private final DualModeMC plugin;
    private final int timeout = 10;
    private final DualModeMCTracker tracker;

    public PlayerListener(DualModeMC plugin) {
        this.plugin = plugin;
        tracker = plugin.getServer().getServicesManager().getRegistration(DualModeMCTracker.class).getProvider();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerLogin(PlayerLoginEvent event) {
        tracker.setPlayerState(event.getPlayer().getName(), PlayerStates.UNKNOWN);
        new bungeeCheck(event.getPlayer()).runTaskLater(plugin, 2);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        tracker.removePlayer(event.getPlayer());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerKick(PlayerKickEvent event) {
        tracker.removePlayer(event.getPlayer());
    }

    class bungeeCheck extends BukkitRunnable {
        Player player;

        public bungeeCheck(Player player) {
            this.player = player;
        }

        @Override
        public void run() {
            ByteArrayOutputStream outB = new ByteArrayOutputStream();
            DataOutputStream out = new DataOutputStream(outB);
            try {
                out.writeUTF("isOnlineMode");
            } catch (IOException e) {
                e.printStackTrace();
            }
            player.sendPluginMessage(PlayerListener.this.plugin, DualModeMC.channelTag, outB.toByteArray());
            new bungeeCheckTimeout(player).runTaskLater(plugin, timeout);
            plugin.getLogger().info(String.format("[DualModeMC] Sending BungeeCheck for player %s", player.getName()));
        }
    }

    class bungeeCheckTimeout extends BukkitRunnable {
        Player player;

        public bungeeCheckTimeout(Player player) {
            this.player = player;
        }

        @Override
        public void run() {
            if (!tracker.getPlayerState(player).equals(PlayerStates.UNKNOWN)) {
                return;
            }
            tracker.changePlayerState(player, plugin.getDMConfig().isOfflinePremiumNamesBlocked() ? PlayerStates.PREMCHECK : PlayerStates.OFFLINE);
        }
    }
}
