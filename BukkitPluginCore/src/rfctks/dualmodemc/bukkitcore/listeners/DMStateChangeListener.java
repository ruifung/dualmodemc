package rfctks.dualmodemc.bukkitcore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import rfctks.dualmodemc.bukkitcore.DualModeMC;
import rfctks.dualmodemc.bukkitcore.PlayerStates;
import rfctks.dualmodemc.bukkitcore.api.DualModeMCTracker;
import rfctks.dualmodemc.bukkitcore.events.DMStateChangeEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class DMStateChangeListener implements Listener {
    DualModeMC plugin;

    public DMStateChangeListener(DualModeMC plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onDMStateChange(DMStateChangeEvent event) {
        if (event.isCancelled()) {
            return;
        }
        String playerState = event.getPlayerState().toString();
        String logText = "[DualModeMC] Player %s is in %s mode.";
        switch (event.getPlayerState()) {
            case OFFLINE:
                plugin.getLogger().info(String.format(logText, event.getPlayer().getName(), playerState));
                break;
            case PREMIUM:
                plugin.getLogger().info(String.format(logText, event.getPlayer().getName(), playerState));
                break;
            case PREMCHECK:
                plugin.getLogger().info(String.format("[DualModeMC] Player %s premium name check pending.", event.getPlayer().getName()));
                new premiumNameCheck(event.getPlayer()).runTaskAsynchronously(plugin);
                break;
        }
    }

    private class premiumNameCheck extends BukkitRunnable {
        private final Player player;
        private final DualModeMCTracker tracker;

        public premiumNameCheck(Player player) {
            this.player = player;
            tracker = plugin.getServer().getServicesManager().getRegistration(DualModeMCTracker.class).getProvider();
        }

        public void run() {
            try {
                if (!tracker.getPlayerState(player).equals(PlayerStates.PREMCHECK)) {
                    return;
                }
                URL url = new URL("https://minecraft.net/haspaid.jsp?user=" + player.getName());
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                String str;
                String reply = null;
                while ((str = in.readLine()) != null) {
                    if (str.equals("false") || str.equals("true")) {
                        reply = str;
                        break;
                    }
                }
                if (reply == null) {
                    return;
                }
                if (reply.equals("true")) {
                    Bukkit.getScheduler().runTask(plugin, new kickPlayer(player, plugin.getDMConfig().getOffOnKickMsg()));
                } else {
                    tracker.changePlayerState(player, PlayerStates.OFFLINE);
                }

            } catch (IOException e) {
                plugin.getLogger().warning(e.toString());
            }
        }
    }

    private class kickPlayer extends BukkitRunnable {
        private Player player;
        private String reason;

        public kickPlayer(Player player, String reason) {
            this.player = player;
            this.reason = reason;
        }

        public void run() {
            player.kickPlayer(reason);
        }
    }
}
