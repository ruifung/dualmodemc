package rfctks.dualmodemc.bukkitcore.listeners;

import org.bukkit.entity.Player;
import rfctks.dualmodemc.bukkitcore.DualModeMC;
import rfctks.dualmodemc.bukkitcore.PlayerStates;
import rfctks.dualmodemc.bukkitcore.api.DualModeMCTracker;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class PluginMessageListener implements org.bukkit.plugin.messaging.PluginMessageListener {
    DualModeMC plugin;

    public PluginMessageListener(DualModeMC plugin) {
        this.plugin = plugin;
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] bytes) {
        if (channel.equals(DualModeMC.channelTag)) {
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(bytes));
            DualModeMCTracker tracker = plugin.getServer().getServicesManager().getRegistration(DualModeMCTracker.class).getProvider();
            try {
                String command = in.readUTF();
                if (command.equals("onlineModeCheck")) {
                    if (!tracker.getPlayerState(player).equals(PlayerStates.UNKNOWN)) {
                        return;
                    }
                    boolean isOnline = in.readBoolean();
                    String sharedSecret = in.readUTF();
                    if (!sharedSecret.equals(plugin.getDMConfig().getSharedSecret())) {
                        plugin.getLogger().warning("[DualModeMC] MISMATCHED SECRET! Please fix your configs!");
                        tracker.changePlayerState(player, PlayerStates.OFFLINE);
                        return;
                    }
                    PlayerStates offlineState = plugin.getDMConfig().isOfflinePremiumNamesBlocked() ? PlayerStates.PREMCHECK : PlayerStates.OFFLINE;
                    tracker.changePlayerState(player, isOnline ? PlayerStates.PREMIUM : offlineState);
                }
            } catch (IOException e) {
                plugin.getLogger().warning(e.toString());
            }
        }
    }
}
