package rfctks.dualmodemc.bukkitcore.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.*;
import rfctks.dualmodemc.bukkitcore.DualModeMC;
import rfctks.dualmodemc.bukkitcore.PlayerStates;
import rfctks.dualmodemc.bukkitcore.api.DualModeMCTracker;

public class PlayerActionsBlocker implements Listener {
    private final DualModeMC plugin;

    public PlayerActionsBlocker(DualModeMC plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        DualModeMCTracker tracker = plugin.getServer().getServicesManager().getRegistration(DualModeMCTracker.class).getProvider();
        PlayerStates playerState = tracker.getPlayerState(event.getPlayer());
        if (playerState.equals(PlayerStates.PREMCHECK) || playerState.equals(PlayerStates.UNKNOWN)) {
            event.setTo(event.getFrom());
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        DualModeMCTracker tracker = plugin.getServer().getServicesManager().getRegistration(DualModeMCTracker.class).getProvider();
        PlayerStates playerState = tracker.getPlayerState(event.getPlayer());
        if (playerState.equals(PlayerStates.PREMCHECK) || playerState.equals(PlayerStates.UNKNOWN)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerCommand(PlayerCommandPreprocessEvent event) {
        DualModeMCTracker tracker = plugin.getServer().getServicesManager().getRegistration(DualModeMCTracker.class).getProvider();
        PlayerStates playerState = tracker.getPlayerState(event.getPlayer());
        if (playerState.equals(PlayerStates.PREMCHECK) || playerState.equals(PlayerStates.UNKNOWN)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerItemPickup(PlayerPickupItemEvent event) {
        DualModeMCTracker tracker = plugin.getServer().getServicesManager().getRegistration(DualModeMCTracker.class).getProvider();
        PlayerStates playerState = tracker.getPlayerState(event.getPlayer());
        if (playerState.equals(PlayerStates.PREMCHECK) || playerState.equals(PlayerStates.UNKNOWN)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerItemDrop(PlayerDropItemEvent event) {
        DualModeMCTracker tracker = plugin.getServer().getServicesManager().getRegistration(DualModeMCTracker.class).getProvider();
        PlayerStates playerState = tracker.getPlayerState(event.getPlayer());
        if (playerState.equals(PlayerStates.PREMCHECK) || playerState.equals(PlayerStates.UNKNOWN)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        DualModeMCTracker tracker;
        if (event.isAsynchronous()) {
            tracker = Bukkit.getServicesManager().getRegistration(DualModeMCTracker.class).getProvider();
        } else {
            tracker = plugin.getServer().getServicesManager().getRegistration(DualModeMCTracker.class).getProvider();
        }
        PlayerStates playerState = tracker.getPlayerState(event.getPlayer());
        if (playerState.equals(PlayerStates.PREMCHECK) || playerState.equals(PlayerStates.UNKNOWN)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent event) {
        DualModeMCTracker tracker = plugin.getServer().getServicesManager().getRegistration(DualModeMCTracker.class).getProvider();
        if (!(event.getPlayer() instanceof Player)) {
            return;
        }
        PlayerStates playerState = tracker.getPlayerState((Player) event.getPlayer());
        if (playerState.equals(PlayerStates.PREMCHECK) || playerState.equals(PlayerStates.UNKNOWN)) {
            event.setCancelled(true);
        }
    }
}
