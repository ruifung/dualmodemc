package rfctks.dualmodemc.bukkitcore.config;

import rfctks.dualmodemc.bukkitcore.DualModeMC;

public class DMCConfig {
    public DMCConfig(DualModeMC plugin) {
        allowOfflinePremium = plugin.getConfig().getBoolean("noOfflinePremiumNames", true);
        sharedSecret = plugin.getConfig().getString("sharedSecret");
        offOnKickMsg = plugin.getConfig().getString("offlinePremiumKickMsg");
    }

    //Config Fields
    private boolean allowOfflinePremium;
    private String sharedSecret;
    private String offOnKickMsg;

    //verify config
    public boolean verify() {
        if (sharedSecret == null) {
            return false;
        } else if (offOnKickMsg == null) {
            return false;
        }
        return true;
    }

    public boolean isOfflinePremiumNamesBlocked() {
        return allowOfflinePremium;
    }

    public String getSharedSecret() {
        return sharedSecret;
    }

    public String getOffOnKickMsg() {
        return offOnKickMsg;
    }
}
