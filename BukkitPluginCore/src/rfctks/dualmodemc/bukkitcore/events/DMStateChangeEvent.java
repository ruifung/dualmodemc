package rfctks.dualmodemc.bukkitcore.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import rfctks.dualmodemc.bukkitcore.PlayerStates;

public class DMStateChangeEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled = false;
    private Player player;
    private PlayerStates playerState;

    public DMStateChangeEvent(Player player, PlayerStates status) {
        this.player = player;
        this.playerState = status;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean b) {
        this.cancelled = b;
    }

    public Player getPlayer() {
        return player;
    }

    public PlayerStates getPlayerState() {
        return playerState;
    }
}
