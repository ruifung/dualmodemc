package rfctks.dualmodemc.bukkitcore;
/*
DualModeMC - Player Tracker
Licensed under GPL 3.0
 */

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import rfctks.dualmodemc.bukkitcore.api.DualModeMCTracker;
import rfctks.dualmodemc.bukkitcore.events.DMStateChangeEvent;

import java.util.HashMap;

public class DualModeMCTrackerProvider implements DualModeMCTracker {
    private DualModeMC plugin;
    private HashMap<String, PlayerStates> players = new HashMap<>();

    //Constructor
    public DualModeMCTrackerProvider(JavaPlugin plugin) {
        if (plugin instanceof DualModeMC) {
            this.plugin = (DualModeMC) plugin;
        }
    }

    /*
     * Gets player status
     * See PlayerStates enum.
     */
    public PlayerStates getPlayerState(String username) {
        if (players.containsKey(username)) {
            return players.get(username);
        } else {
            return PlayerStates.NOPLAYER;
        }
    }

    public PlayerStates getPlayerState(Player player) {
        return getPlayerState(player.getName());
    }

    /*
     * Sets player status
     * See PlayerStates enum.
     * Does not fire event.
     */
    public void setPlayerState(String username, PlayerStates status) {
        players.put(username, status);
    }

    public void setPlayerState(Player player, PlayerStates status) {
        setPlayerState(player.getName(), status);
    }

    /*
     * Changes player state,
     * Fires DMStateChangeEvent.
     */
    public void changePlayerState(String username, PlayerStates state) {
        changePlayerState(plugin.getServer().getPlayer(username), state);
    }

    public void changePlayerState(Player player, PlayerStates state) {
        players.put(player.getName(), state);
        DMStateChangeEvent event = new DMStateChangeEvent(player, state);
        plugin.getServer().getPluginManager().callEvent(event);
    }

    /*
     * Remove player from tracker
     */
    public boolean removePlayer(String username) {
        if (players.containsKey(username)) {
            players.remove(username);
            return true;
        } else {
            return false;
        }
    }

    public boolean removePlayer(Player player) {
        return removePlayer(player.getName());
    }
}
