package rfctks.dualmodemc.bukkitcore;

import org.bukkit.event.HandlerList;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;
import rfctks.dualmodemc.bukkitcore.api.DualModeMCTracker;
import rfctks.dualmodemc.bukkitcore.config.DMCConfig;
import rfctks.dualmodemc.bukkitcore.listeners.DMStateChangeListener;
import rfctks.dualmodemc.bukkitcore.listeners.PlayerActionsBlocker;
import rfctks.dualmodemc.bukkitcore.listeners.PlayerListener;
import rfctks.dualmodemc.bukkitcore.listeners.PluginMessageListener;

public class DualModeMC extends JavaPlugin {
    DualModeMCTrackerProvider playerTracker;
    public static final String channelTag = "rfctks.dmmc";
    private DMCConfig config;

    @Override
    public void onEnable() {
        if (this.getServer().getOnlineMode()) {
            this.setEnabled(false);
            this.getLogger().info("DualModeMC has no use on a online-mode=true server.");
            return;
        }
        this.saveDefaultConfig();
        config = new DMCConfig(this);
        if (!config.verify()) {
            this.setEnabled(false);
            config = null;
            this.getLogger().warning("Config verification failed. DualModeMC-BukkitPlugin disabled!");
            return;
        }
        playerTracker = new DualModeMCTrackerProvider(this);
        this.getServer().getServicesManager().register(DualModeMCTracker.class, this.playerTracker, this, ServicePriority.Normal);
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, channelTag);
        this.getServer().getMessenger().registerIncomingPluginChannel(this, channelTag, new PluginMessageListener(this));
        this.getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
        this.getServer().getPluginManager().registerEvents(new DMStateChangeListener(this), this);
        this.getServer().getPluginManager().registerEvents(new PlayerActionsBlocker(this), this);
        this.getLogger().info("DualModeMC-BukkitPlugin enabled.");
    }

    @Override
    public void onDisable() {
        this.getServer().getMessenger().unregisterIncomingPluginChannel(this, channelTag);
        this.getServer().getMessenger().unregisterOutgoingPluginChannel(this, channelTag);
        this.getServer().getServicesManager().unregisterAll(this);
        HandlerList.unregisterAll(this);
        this.saveConfig();
    }

    public DMCConfig getDMConfig() {
        return config;
    }
}
