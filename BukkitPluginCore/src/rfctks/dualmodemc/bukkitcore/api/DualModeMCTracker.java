package rfctks.dualmodemc.bukkitcore.api;

import org.bukkit.entity.Player;
import rfctks.dualmodemc.bukkitcore.PlayerStates;

public interface DualModeMCTracker {
    // Get player status, see PlayerStates ENUM.
    public PlayerStates getPlayerState(String username);

    public PlayerStates getPlayerState(Player player);

    // Sets player status, see PlayerStates ENUM.
    public void setPlayerState(String username, PlayerStates state);

    public void setPlayerState(Player player, PlayerStates state);

    // Changes player state, make sure to fire DMStateChangeEvent.
    public void changePlayerState(String username, PlayerStates state);

    public void changePlayerState(Player player, PlayerStates state);

    // Removes player from tracker, usually called on logout.
    public boolean removePlayer(String username);

    public boolean removePlayer(Player player);
}
