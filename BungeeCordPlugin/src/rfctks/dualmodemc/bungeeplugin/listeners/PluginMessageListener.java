package rfctks.dualmodemc.bungeeplugin.listeners;
/*
Message structure
- String Subcommand
- Command parameters

Valid commands:
- isOnlineMode (NO PARAMETERS)
- REPLY: onlineModeCheck (Boolean)
 */

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;
import rfctks.dualmodemc.bungeeplugin.DualModeMC;

import java.io.*;

public class PluginMessageListener implements Listener{
    private DualModeMC plugin;
    public PluginMessageListener(Plugin plugin) {
        if (plugin instanceof DualModeMC) {
            this.plugin = (DualModeMC) plugin;
            plugin.getLogger().info("PluginMessageListener registered.");
        }
    }

    @EventHandler
    public void pluginMessageHandler(PluginMessageEvent event) {
        if (event.getTag().equals(DualModeMC.channelTag)) {
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(event.getData()));
            try {
                String command = in.readUTF();
                if (command.equals("isOnlineMode")) {
                    ByteArrayOutputStream outB = new ByteArrayOutputStream();
                    DataOutputStream out = new DataOutputStream(outB);
                    out.writeUTF("onlineModeCheck");
                    out.writeBoolean(plugin.getMode());
                    out.writeUTF(plugin.getConfig().getSharedSecret());
                    if (event.getSender() instanceof Server) {
                        Server target = (Server) event.getSender();
                        target.sendData(DualModeMC.channelTag,outB.toByteArray());
                    }
                    ProxyServer.getInstance().getLogger().info(String.format("[DualModeMC] BungeeCheck received."));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}
