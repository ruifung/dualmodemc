package rfctks.dualmodemc.bungeeplugin.config;

import net.cubespace.Yamler.Config.Comments;
import net.cubespace.Yamler.Config.Config;
import rfctks.dualmodemc.bungeeplugin.DualModeMC;

import java.io.File;

public class DMConfig extends Config {
    public DMConfig(DualModeMC plugin) {
        CONFIG_HEADER = new String[]{"Dual Mode MC - BungeeCord Plugin config"};
        CONFIG_FILE = new File(plugin.getDataFolder(),"dmMC-bungee.yml");
    }

    @Comments({
        "This is the shared secret used to verify that this bungeecord instance is allowed to check users.",
        "This is required to verify that a player isn't attempting to spoof the premium check to bypass login.",
        "This must be same on both server and bungee."
    })
    private String sharedSecret = "changeMeBeforeUse";

    public String getSharedSecret() {
        return sharedSecret;
    }

}
