package rfctks.dualmodemc.bungeeplugin;

import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import rfctks.dualmodemc.bungeeplugin.config.DMConfig;
import rfctks.dualmodemc.bungeeplugin.listeners.PluginMessageListener;

import java.util.logging.Logger;

public class DualModeMC extends Plugin{
    private boolean isOnlineMode;
    public static final String channelTag = "rfctks.dmmc";
    private DMConfig config;

    @Override
    public void onEnable() {
        config = new DMConfig(this);
        try {
            config.init();
            config.save();
        } catch (InvalidConfigurationException e) {
            Logger log = ProxyServer.getInstance().getLogger();
            log.severe("Config error!");
            log.severe(e.toString());
            return;
        }
        isOnlineMode = ProxyServer.getInstance().getConfig().isOnlineMode();
        ProxyServer.getInstance().registerChannel(channelTag);
        ProxyServer.getInstance().getPluginManager().registerListener(this,new PluginMessageListener(this));
    }

    public boolean getMode() {
        return isOnlineMode;
    }
    public DMConfig getConfig() {
        return config;
    }
}
